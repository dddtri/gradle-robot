*** Settings ***
Documentation     A test suite with a single test for google search on Ruckus Wireless
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resources-htmlunit.robot

*** Test Cases ***
Google Search For "Ruckus Wireless"
    Register Keyword To Run On Failure   Log Source
    Open Browser To Google Page
    Input Search Term    Ruckus Wireless
    Click Search
    Check Result Found
    [Teardown]    Close Browser