*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${GOOGLE URL}         http://www.google.com
${BROWSER}        htmlunit
${DELAY}          5



*** Keywords ***
Open Browser To Google Page
    Open Browser    ${GOOGLE URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Google Page Should Be Open

Google Page Should Be Open
    Title Should Be    Google

Go To Login Page
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Input Search Term
    [Arguments]    ${searchTerm}
    Input Text     q   ${searchTerm}

Click Search
    Click Element   btnG

Check Result Found
    Page Should Contain Element   resultStats


