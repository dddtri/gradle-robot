# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* an example on leveraging robot framework as test runner and Selenium2Library builtin keywords to script "goole search" automation testcase


### How do I get set up? ###

* Summary of set up
    1.  java is required

* Configuration
    1  see build.gradle

* Dependencies
    1. see build.gradle

* Database configuration

* How to run the automation test locally
    1. check out sourcecode
    2. open up command window and cd to root folder
    3. enter gradlew test --info

* Docker Image Build
    1. Open command window and enter the commands in following steps
    2. cd %rootFolder%
    3. gradlew clean install --info
    4. cd %rootFolder%/build/libs
    5. docker build --tag=ruckus/robot ./

* Docker Container Deployment instruction
    1. download ruckus-robot-container.tar
    2. open up command console
    3. enter docker import ruckus-robot-container.tar ruckus/robot-container:new
    5. enter docker run -p 8080:5050 ruckus/robot-container:new java -jar /app/robot-all.jar --outputdir /app/results /app/tests